package com.example.vend.listviewmvvmproject;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    ApiService provideApiService(){
        return new ApiService();
    }

    @Provides
    @Singleton
    ListViewModel provideListViewModel(){
        return new ListViewModel();
    }

}
