package com.example.vend.listviewmvvmproject;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ListViewModel extends ViewModel {

    private ApiService mService;
    private MutableLiveData<List<Movie>> mMovieList;

    public void initListViewModel(ApiService service) {
        this.mService = service;
        //fetchData();
    }

    public void fetchData() {

        mService.getServiceData(new ApiService.CallBack() {
            @Override
            public void onSuccess(List<Movie> movies) {
                if (mMovieList == null){mMovieList = new MutableLiveData<>();}
                mMovieList.setValue(movies);

            }

            @Override
            public void onFailure() {
                Log.e("MyActivity","Response failed");
            }
        });
    }

    public LiveData<List<Movie>> getMovieList(){
        if (mMovieList == null){
            mMovieList = new MutableLiveData<>();
            fetchData();
        }
        return mMovieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.mMovieList.postValue(movieList);
    }
}
