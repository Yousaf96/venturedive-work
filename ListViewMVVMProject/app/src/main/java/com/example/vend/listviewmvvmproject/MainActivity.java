package com.example.vend.listviewmvvmproject;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vend.listviewmvvmproject.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.inject.Inject;


public class MainActivity extends AppCompatActivity{

    public static final String EXTRA_DATA = "com.example.vend.listproject.DATA";
    private static final String API_KEY = "da60f078491335fb68f642093c5ee763";

    private ActivityMainBinding activityMainBinding;

    private List<Movie> mMovieList = new ArrayList<>();
    MovieArrayAdapter movieAdapter;
    @Inject ListViewModel mListViewModel;
    @Inject ApiService mService;

    private AppComponent mAppComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);

        if (mAppComponent == null){
            mAppComponent = DaggerAppComponent.builder().appModule(new AppModule()).build();
        }
        mAppComponent.inject(this);

        if(API_KEY.isEmpty()){
            Toast.makeText(getApplicationContext(),"Obtain API KEY",Toast.LENGTH_LONG).show();
            return;
        }

        mListViewModel = ViewModelProviders.of(this).get(ListViewModel.class);

        mListViewModel.initListViewModel(mService);



        mListViewModel.getMovieList().observe(this, new android.arch.lifecycle.Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movies) {
                if (movies != null){
                    mMovieList = movies;
                    setAdapterData(movies);
                }
            }
        });


        activityMainBinding.mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                sendDataToNextScreen(mMovieList.get(i));
            }
        });

    }


    private void sendDataToNextScreen(Movie movie) {

        Intent intent = new Intent(this,MovieDescriptionActivity.class);
        intent.putExtra(EXTRA_DATA,movie);
        startActivity(intent);

    }

    public void setAdapterData(@NonNull List<Movie> movies) {

        activityMainBinding.progressBar.setVisibility(View.VISIBLE);
        if (movies == null || movies.isEmpty()){
            Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
        }
        movieAdapter = new MovieArrayAdapter(MainActivity.this, R.layout.adapter_view_layout, movies);
        activityMainBinding.mainListView.setAdapter(movieAdapter);
        Log.e("MyActivity", movies.toString());
        activityMainBinding.progressBar.setVisibility(View.GONE);

    }

}
