package com.example.vend.listviewmvvmproject;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

public class Movie implements Serializable {

    @SerializedName("original_title")
    private String originalTitle;
    @SerializedName("overview")
    private String overview;
    @SerializedName("poster_path")
    private String poster_path;

    public Movie(String originalTitle, String overview, String poster_path) {
        this.originalTitle = originalTitle;
        this.overview = overview;
        this.poster_path = poster_path;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }
}
