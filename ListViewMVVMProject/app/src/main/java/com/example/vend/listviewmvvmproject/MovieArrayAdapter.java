package com.example.vend.listviewmvvmproject;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.vend.listviewmvvmproject.databinding.AdapterViewLayoutBinding;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieArrayAdapter extends ArrayAdapter<Movie> {

    private Context mContext;
    int mResource;
    public static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500";

    public MovieArrayAdapter(@NonNull Context context, int resource, @NonNull List<Movie> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        AdapterViewLayoutBinding movieRowBinding = DataBindingUtil.inflate(inflater ,mResource, parent,false);//AdapterViewLayoutBinding.inflate(inflater);

        movieRowBinding.listTitleText.setText(getItem(position).getOriginalTitle());
        movieRowBinding.listDescriptionText.setText(getItem(position).getOverview());
        Picasso.with(mContext)
                .load(IMAGE_URL+getItem(position).getPoster_path())
                .resize(400,600)
                .into(movieRowBinding.listImage);

        return movieRowBinding.getRoot();
    }

}
