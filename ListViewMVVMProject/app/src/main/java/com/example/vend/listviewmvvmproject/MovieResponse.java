package com.example.vend.listviewmvvmproject;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieResponse {
    @SerializedName("results")
    public List<Movie> results;
    @SerializedName("total_results")
    private int total_results;


    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
