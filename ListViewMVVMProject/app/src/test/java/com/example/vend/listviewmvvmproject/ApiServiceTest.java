package com.example.vend.listviewmvvmproject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApiServiceTest {


    MovieResponse testResponse;

    @Mock ApiService.CallBack mockCallBack;
    @Mock Call<MovieResponse> mockCall;
    @Mock
    ApiClientWrapper mockWrapper;
    @Mock
    ApiInterface mockApiInterface;
    @Captor ArgumentCaptor<Callback<MovieResponse>> callBackCaptor;

    @InjectMocks ApiService mockService;

    @Before
    public void setUp(){
        testResponse = new MovieResponse();
        testResponse.setResults(Arrays.asList(new Movie("title","overview","jdkjelfe")));
    }

    @Test
    public void checkApiInterface_InitialisedOnce(){
        when(mockWrapper.initInterface()).thenReturn(mockApiInterface);
        when(mockApiInterface.getTopRatedMovies("da60f078491335fb68f642093c5ee763")).thenReturn(mockCall);
        mockService.getServiceData(mockCallBack);
        verify(mockWrapper, times(1)).initInterface();
    }

    @Test
    public void testResponse_OnSuccess(){
        when(mockWrapper.initInterface()).thenReturn(mockApiInterface);
        when(mockApiInterface.getTopRatedMovies("da60f078491335fb68f642093c5ee763")).thenReturn(mockCall);

        mockService.getServiceData(mockCallBack);
        verify(mockCall).enqueue(callBackCaptor.capture());
        callBackCaptor.getValue().onResponse(mockCall, Response.success(testResponse));

        verify(mockCallBack).onSuccess(eq(testResponse.getResults()));


    }

    @Test
    public void testResponse_OnFailure(){
        when(mockWrapper.initInterface()).thenReturn(mockApiInterface);
        when(mockApiInterface.getTopRatedMovies("da60f078491335fb68f642093c5ee763")).thenReturn(mockCall);

        mockService.getServiceData(mockCallBack);
        verify(mockCall).enqueue(callBackCaptor.capture());
        callBackCaptor.getValue().onFailure(mockCall, new Throwable());

        verify(mockCallBack).onFailure();


    }
}