package com.example.vend.listviewproject;

public class ApiClientWrapper {

    public ApiInterface initInterface(){
        return ApiClient.getClient().create(ApiInterface.class);
    }

}
