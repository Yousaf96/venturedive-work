package com.example.vend.listviewproject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiService{

    public static final String API_KEY = "da60f078491335fb68f642093c5ee763";
    private ApiClientWrapper wrapper = new ApiClientWrapper();

    public void getServiceData(final CallBack callBack){
        final ApiInterface service = wrapper.initInterface();
        Call<MovieResponse> call = service.getTopRatedMovies(API_KEY);
        call.enqueue(new Callback<MovieResponse>(){


            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {

                if (response.isSuccessful()){
                    if (response.body().getResults().isEmpty() || response.body().getResults() == null){
                        callBack.onFailure();
                    }else{
                        callBack.onSuccess(response.body().getResults());
                    }

                }else {
                    callBack.onFailure();
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                callBack.onFailure();
            }
        });
    }
    public interface CallBack{
        void onSuccess(List<Movie> movies);

        void onFailure();
    }
}
