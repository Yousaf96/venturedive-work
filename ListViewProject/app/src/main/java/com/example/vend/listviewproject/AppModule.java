package com.example.vend.listviewproject;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    ApiService provideApiService(){
        return new ApiService();
    }

    @Provides
    @Singleton
    MainPresenter provideMainPresenter(){
        return new MainPresenter();
    }

}
