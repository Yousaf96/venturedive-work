package com.example.vend.listviewproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class MainActivity extends AppCompatActivity implements MainActivityContract.MvpView {

    public static final String EXTRA_DATA = "com.example.vend.listproject.DATA";
    private static final String API_KEY = "da60f078491335fb68f642093c5ee763";

    private List<Movie> mMovieList = new ArrayList<>();
    private ListView mListView;
    private ProgressBar mProgress;
    @Inject MainPresenter mPresenter;
    @Inject ApiService mService;

    private AppComponent mAppComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (mAppComponent == null){
            mAppComponent = DaggerAppComponent.builder().appModule(new AppModule()).build();
        }
        mAppComponent.inject(this);

        if(API_KEY.isEmpty()){
            Toast.makeText(getApplicationContext(),"Obtain API KEY",Toast.LENGTH_LONG).show();
            return;
        }

        mPresenter.initPresenter(this, mService);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                sendDataToNextScreen(mMovieList.get(i));
            }
        });

    }


    private void sendDataToNextScreen(Movie movie) {

        Intent intent = new Intent(this,MovieDescriptionActivity.class);
        intent.putExtra(EXTRA_DATA,movie);
        startActivity(intent);

    }

    @Override
    public void initView() {
        mListView = (ListView) findViewById(R.id.main_listView);
        mProgress = (ProgressBar) findViewById(R.id.progress_bar);
    }

    @Override
    public void setAdapterData(@NonNull List<Movie> movies) {

        mProgress.setVisibility(View.VISIBLE);
        MainActivity.this.mMovieList = movies;
        if (movies == null || movies.isEmpty()){
            Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
        }
        MovieArrayAdapter adapter = new MovieArrayAdapter(MainActivity.this, R.layout.adapter_view_layout, movies);
        MainActivity.this.mListView.setAdapter(adapter);
        mProgress.setVisibility(View.INVISIBLE);

    }
}
