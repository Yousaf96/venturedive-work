package com.example.vend.listviewproject;

import java.util.List;

public interface MainActivityContract {

    interface MvpView{
        void initView();
        void setAdapterData(List<Movie> movieList);


    }

    interface Presenter{
        void initPresenter(MvpView view, ApiService service);
        void fetchData();

    }
}
