package com.example.vend.listviewproject;

import android.util.Log;
import java.util.List;

import javax.inject.Inject;

public class MainPresenter implements MainActivityContract.Presenter {

    private MainActivityContract.MvpView mView;
    @Inject ApiService mService;



    @Override
    public void initPresenter(MainActivityContract.MvpView mView, ApiService service) {
        this.mView = mView;
        this.mService = service;
        mView.initView();
        fetchData();
    }

    @Override
    public void fetchData() {

        mService.getServiceData(new ApiService.CallBack() {
            @Override
            public void onSuccess(List<Movie> movies) {
                mView.setAdapterData(movies);
            }

            @Override
            public void onFailure() {
                Log.e("MyActivity","Response failed");
            }
        });
    }
}
