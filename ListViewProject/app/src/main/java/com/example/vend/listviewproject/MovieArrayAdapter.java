package com.example.vend.listviewproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieArrayAdapter extends ArrayAdapter<Movie> {

    private Context mContext;
    int mResource;
    public static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500";

    public MovieArrayAdapter(@NonNull Context context, int resource, @NonNull List<Movie> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(mContext);

        if (convertView == null){
            convertView = inflater.inflate(mResource, parent,false);
        }

        setDataInView(getMovieData(position), convertView);

        return convertView;
    }

    private Movie getMovieData(int position){

        Movie movie = new Movie(getItem(position).getOriginalTitle(), getItem(position).getOverview(), getItem(position).getPoster_path());
        Log.e("MyActivity","Return movies");
        return movie;
    }

    @NonNull
    private void setDataInView(Movie movie, View convertView){

        TextView tTitle = (TextView) convertView.findViewById(R.id.list_titleText);
        TextView tDescription = (TextView) convertView.findViewById(R.id.list_descriptionText);
        ImageView iImage = (ImageView) convertView.findViewById(R.id.list_image);

        tTitle.setText(movie.getOriginalTitle());
        tDescription.setText(movie.getOverview());
        Picasso.with(mContext)
                .load(IMAGE_URL+movie.getPoster_path())
                .resize(400,600)
                .into(iImage);

    }
}
