package com.example.vend.listviewproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import static com.example.vend.listviewproject.MainActivity.EXTRA_DATA;

public class MovieDescriptionActivity extends AppCompatActivity {

    public static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_description);

        Movie movie = (Movie) getIntent().getSerializableExtra(EXTRA_DATA);

        TextView titleText = findViewById(R.id.movie_title);
        TextView descText = findViewById(R.id.movie_description);
        ImageView image = findViewById(R.id.movie_image);

        titleText.setText(movie.getOriginalTitle());
        descText.setText(movie.getOverview());
        Picasso.with(this)
                .load(IMAGE_URL+movie.getPoster_path())
                .resize(600,900)
                .into(image);

    }


}
