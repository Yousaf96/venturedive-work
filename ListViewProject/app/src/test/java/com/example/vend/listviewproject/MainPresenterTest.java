package com.example.vend.listviewproject;

import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    @Mock MainActivityContract.MvpView mockView;
    @Mock ApiService mockService;
    @Mock ApiService.CallBack mockCallBack;
    List<Movie> testMovies = new ArrayList<>();
    @Captor ArgumentCaptor<ApiService.CallBack> callBackCaptor;

    @InjectMocks MainPresenter mockPresenter;


    @Before
    public void setUp(){
        testMovies.add(new Movie("title","overview","jdkjelfe"));
    }

    @Test
    public void adapterDataSet_onSuccess(){

        mockPresenter.initPresenter(mockView, mockService);
        verify(mockService).getServiceData(callBackCaptor.capture());
        callBackCaptor.getValue().onSuccess(testMovies);

        verify(mockView).setAdapterData(testMovies);
    }
}