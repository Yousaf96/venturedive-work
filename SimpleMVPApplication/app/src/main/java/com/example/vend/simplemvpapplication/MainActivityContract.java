package com.example.vend.simplemvpapplication;

import android.view.View;

public interface MainActivityContract {

    interface MvpView{

        void initView();
        String getViewData();
        void setViewData(String name);

    }
    interface Presenter{
        void onSubmitClick(View view);
        void updateTextView();

    }
    interface Model{

        String getData();
        void setData(String name);
    }
}
