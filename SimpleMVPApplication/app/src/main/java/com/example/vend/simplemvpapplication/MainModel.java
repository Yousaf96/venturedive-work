package com.example.vend.simplemvpapplication;

import java.util.ArrayList;

public class MainModel implements MainActivityContract.Model {

    private MovieList mMovieList;

    @Override
    public String getData() {
        if (mMovieList == null){
            return null;
        }
        return mMovieList.getMovie_item();
    }

    @Override
    public void setData(String name) {
        if (mMovieList == null){
            ArrayList<String> movie_list = new ArrayList<>();
            mMovieList = new MovieList(movie_list);
        }
        mMovieList.addMovie(name);
    }
}
