package com.example.vend.simplemvpapplication;

import android.view.View;

public class MainPresenter implements MainActivityContract.Presenter {

    private MainActivityContract.MvpView mView;
    private MainModel mModel;

    public MainPresenter(MainActivityContract.MvpView view) {
        this.mView = view;
        initPresenter();
    }

    private void initPresenter(){
        mModel = new MainModel();
        mView.initView();
    }

    @Override
    public void onSubmitClick(View view) {
        mModel.setData(mView.getViewData());
        updateTextView();
    }

    @Override
    public void updateTextView() {
        mView.setViewData(mModel.getData());
    }
}
