package com.example.vend.simplemvpapplication;

import java.util.ArrayList;

public class MovieList {

    private ArrayList<String> movie_list;

    public MovieList(ArrayList<String> movie_list) {
        this.movie_list = movie_list;
    }

    public String getMovie_item() {
        return movie_list.get(movie_list.size()-1);
    }

    public void addMovie(String movie) {
        this.movie_list.add(movie);
    }
}
