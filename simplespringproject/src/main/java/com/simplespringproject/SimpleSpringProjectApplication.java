package com.simplespringproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@ComponentScan(basePackages="com.simplespringproject")
@SpringBootApplication
public class SimpleSpringProjectApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SimpleSpringProjectApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SimpleSpringProjectApplication.class, args);
    }
}
