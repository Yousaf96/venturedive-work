package com.simplespringproject.controller;

import com.simplespringproject.model.Role;
import com.simplespringproject.model.User;
import com.simplespringproject.service.UserServiceImpl;
import com.simplespringproject.validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserValidator userValidator;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        List<Role> roles = new ArrayList<>();
        roles.addAll(Arrays.asList(Role.values()));
        model.addAttribute("roles",roles);

        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        log.info(userForm.getUserName());

        model.addAttribute("successMessage", "User has been registered successfully");

        return "registration";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.info("getting the user from username");
        User user = userService.findByUserName(auth.getName());
        model.addAttribute("user", user);
        if (user.getRole() == Role.ADMIN){
            List<User> userList = new ArrayList<>();
            userService.findAllUsers().forEach(userList::add);
            model.addAttribute("userList", userList);
        }

        return "welcome";
    }
}
