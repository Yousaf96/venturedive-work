package com.simplespringproject.model;

public enum Role{
    ADMIN, NORMAL;
}
