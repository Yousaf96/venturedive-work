package com.simplespringproject.service;

import com.simplespringproject.model.User;

import java.util.List;

public interface UserService {
    void save(User user);

    User findByUserName(String userName);

    Iterable<User> findAllUsers();
}
