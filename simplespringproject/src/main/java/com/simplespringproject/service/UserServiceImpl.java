package com.simplespringproject.service;

import com.simplespringproject.model.User;
import com.simplespringproject.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void save(User user){
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userRepository.save(user);

	}

	@Override
	public User findByUserName(String username){
		return userRepository.findByUserName(username);
	}

	@Override
	public Iterable<User> findAllUsers() {
		return userRepository.findAll();
	}

}
